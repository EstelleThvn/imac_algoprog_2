#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int capacite;
    int taille;
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == NULL){
        return true;
    }
    else {
        return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nouveau = (Noeud*)malloc(sizeof(Noeud));
    nouveau->donnee = valeur;
    nouveau->suivant = NULL;

    //si le premier noeud de la liste est vide, on lui donne une valeur
    if(liste->premier == NULL){
        liste->premier = nouveau;
    }
    //sinon, on cherche le dernier élément de la liste et on le rajoute à la fin de la structure
    else {
        Noeud* suite = liste->premier;
        while(suite->suivant != NULL){
            suite = suite->suivant;
        }
        suite->suivant = nouveau;
    }
}

void affiche(const Liste* liste)
{
    Noeud* suite = liste->premier;
    while(suite->suivant != NULL){
        cout << suite->donnee << " ; ";
        suite = suite->suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    Noeud* suite = liste->premier;
    int valeur;
    for(int i=0; i<n; i++){
        suite = suite->suivant;
    }
    if(suite->donnee != NULL) {
        valeur = suite->donnee;
    }
    else{
        valeur = 0;
    }

    return valeur;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* suite = liste->premier;
    int index = -1;

    int i=0;

    while(suite->suivant != NULL){
        if(suite->donnee == valeur) {
            index = i;
            break;
        }
        suite = suite->suivant;
        i++;
    }

    return index;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* suite = liste->premier;
    for(int i=0; i<n; i++){
        suite = suite->suivant;
    }
    if(suite->donnee != NULL) {
        suite->donnee = valeur;
    }
    else{
        cout << "l'indice ne fait pas partie du tableau" << endl;
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{

    if(tableau->taille < tableau->capacite){
        tableau->donnees[tableau->taille] = valeur;
        tableau->taille++;
    }
    else{
        tableau->donnees = (int*)realloc( tableau->donnees, sizeof(int));
        tableau->capacite++;
        tableau->donnees[tableau->taille] = valeur;
        tableau->taille++;
    }

}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees = (int*) malloc(sizeof(int)*capacite);
    tableau->capacite = capacite;
    tableau->taille = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if(liste->donnees == NULL){
        return true;
    }
    else {
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->taille ; i++){
        cout << tableau->donnees[i] << " ; ";
    }
    cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    int valeur;
    if(n>tableau->taille){
        cout << "cet index ne fait pas partie du tableau" << endl;
        valeur = 0;
    }
    else {
        valeur = tableau->donnees[n];
    }
    return valeur;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int index = 0;
    while(index<tableau->taille){
        if(tableau->donnees[index] == valeur){
            return index;
        }
        index++;
    }
    return -1;


    /* int index = -1;
    for(int i=0; i<tableau->taille ; i++){
        if(tableau->donnees[i] == valeur){
            index = i;
            break;
        }
    }
    return index;
    */
}

void stocke(DynaTableau* tableau, int n, int valeur)
{

    if(n>tableau->taille){
        cout << "cet index ne fait pas partie du tableau" << endl;
    }
    else {
        tableau->donnees[n] = valeur;
    }

}

void pousse_file(DynaTableau* liste, int valeur)
//void pousse_file(Liste* liste, int valeur)
{
    //on ajoute à la fin de la liste
    ajoute(liste, valeur);
}

int retire_file(DynaTableau* liste)
//int retire_file(Liste* liste)
{
    //comme on a ajouté à la fin de la liste, on retire la première valeur de la liste (au début)

    int valeur = 0;

    if(liste->donnees!= NULL){
        valeur = liste->donnees[0];
        for(int i=0; i<liste->taille-1;i++){
            liste->donnees[i]=liste->donnees[i+1];
        }
        liste->donnees[liste->taille-1]=NULL;
        liste->taille--;
    }

    return valeur;
}

//void pousse_pile(Liste* liste, int valeur)
void pousse_pile(DynaTableau* liste, int valeur)
{
    //on ajoute à la fin de la liste
    ajoute(liste, valeur);
}

int retire_pile(DynaTableau* liste)
//int retire_pile(Liste* liste)
{
    //comme on a ajouté à la fin, on retire à la fin de la liste aussi

    int valeur = 0;

    if(liste->donnees!= NULL){
        valeur = liste->donnees[liste->taille-1];
        liste->donnees[liste->taille-1]=NULL;
        liste->taille--;
    }

    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    //Liste pile;
    DynaTableau pile;
    //Liste file;
    DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&liste) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}

#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    int leftChildIndex = nodeIndex*2+1;

    return leftChildIndex;
}

int Heap::rightChild(int nodeIndex)
{
    int rightChildIndex = nodeIndex*2+2;

    return rightChildIndex;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    //this->get(i) = value;
    this->set(i,value);

    while(i>0 && this->get(i) > this->get((i-1)/2)) {
        this->swap(i,(i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i = nodeIndex;

	int i_max = nodeIndex;
    if(leftChild(i)< heapSize && this->get(leftChild(i))>this->get(i_max)){
        i_max = this->leftChild(i);
    }
    if(rightChild(i)< heapSize &&this->get(rightChild(i))>this->get(i_max)){
        i_max = this->rightChild(i);
    }

    if(i_max != i) {
        this->swap(i,i_max);
        this->heapify(heapSize,i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i=0; i<numbers.size() ; i++){
        this->insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    for(int i=this->size()-1; i>0 ; i--){
        this->swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}

#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if(size>1){
        Array& lowerArray = w->newArray(size);
        Array& greaterArray= w->newArray(size);
        int lowerSize = 0, greaterSize = 0; // effectives sizes

        // split
        int pivot = toSort[0];
        //on fait i=1 parce qu'il y a le pivot au début donc on en a un de moins à faire
        for(int i=1 ; i<size ; i++){
            if(toSort[i]<=pivot){
                lowerArray[lowerSize] = toSort[i];
                lowerSize++;
            }
            else{
                greaterArray[greaterSize] = toSort[i];
                greaterSize++;
            }
        }

        // recursiv sort of lowerArray and greaterArray
        recursivQuickSort(lowerArray, lowerSize);
        recursivQuickSort(greaterArray, greaterSize);

        // merge
        bool pivotInsere = false;
                int j = 0;
                for(int i = 0; i< size; i++){
                    if(i < lowerSize){
                        toSort[i] = lowerArray[i];
                    } else{
                        if(pivotInsere == false){
                            pivotInsere = true;
                            toSort[i] = pivot;

                        }
                        else {
                            toSort[i] = greaterArray[j];
                            j++;
                        }
                    }

                }

    }
    else {
        return;
    }
}

void quickSort(Array& toSort){
    recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    uint elementCount=20;
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
    w->show();

    return a.exec();
}

#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left = NULL;
        this->right = NULL;
        this->value = value;

    }

    void insertNumber(int value) {
        // create a new node and insert it in right or left child

        if(value<this->value){
            if(this->left == NULL){
                Node* nouveauLeft = createNode(value);
                nouveauLeft->initNode(value);
                this->left = nouveauLeft;
            }
            else {
                this->left->insertNumber(value);
            }
        }
        else {
            if(this->right == NULL){
                Node* nouveauRight = createNode(value);
                nouveauRight->initNode(value);
                this->right = nouveauRight;
            }
            else {
                this->right->insertNumber(value);
            }
        }
    }

    uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        uint leftheight = 1;
        uint rightheight = 1;

        if(this->left!=NULL){
            leftheight += this->left->height();
        }
        if(this->right!=NULL){
            rightheight += this->right->height();
        }

        if (leftheight > rightheight){
            return leftheight;
        }
        else {
            return rightheight;
        }

    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint NodeNb = 1;

        if(this->left!=NULL){
            NodeNb += this->left->nodesCount();
        }
        if(this->right!=NULL){
            NodeNb += this->right->nodesCount();
        }

        return NodeNb;

    }

    bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left==NULL && this->right==NULL){
            return true;
        }
        else {
            return false;
        }
    }

    void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else {
            if(this->left!=NULL){
                this->left->allLeaves(leaves, leavesCount);
            }
            if(this->right!=NULL){
                this->right->allLeaves(leaves,leavesCount);
            }
        }

    }

    void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left!=NULL){
            this->left->inorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;
        if(this->right!=NULL){
            this->right->inorderTravel(nodes, nodesCount);
        }
    }

    void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;
        if(this->left!=NULL){
            this->left->preorderTravel(nodes, nodesCount);
        }
        if(this->right!=NULL){
            this->right->preorderTravel(nodes, nodesCount);
        }
    }

    void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if(this->left!=NULL){
            this->left->postorderTravel(nodes, nodesCount);
        }
        if(this->right!=NULL){
            this->right->postorderTravel(nodes, nodesCount);
        }
        nodes[nodesCount]=this;
        nodesCount++;
    }

    Node* find(int value) {
        // find the node containing value
        Node* nodeValue = NULL;
        if(this->value == value){
            nodeValue = this;
        }

        if (value < this->value) {
            if(this->left!=NULL){
                nodeValue = this->left->find(value);
            }
        }
        if (value > this->value) {
            if(this->right!=NULL){
                nodeValue = this->right->find(value);
            }
        }
        return nodeValue;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
    w->show();

    return a.exec();
}
